import numpy as np 

class GeneticAlgorithm(object): 
	"""
		Implement a simple generationl genetic algorithm as described in the instructions
	"""

	def __init__(	self, chromosomeShape,
					errorFunction,
					elitism = 1,
					populationSize = 25, 
					mutationProbability  = .1, 
					mutationScale = .5,
					numIterations = 10000, 
					errorTreshold = 1e-6
					): 

		self.populationSize = populationSize # size of the population of units
		self.p = mutationProbability # probability of mutation
		self.numIter = numIterations # maximum number of iterations
		self.e = errorTreshold # threshold of error while iterating
		self.f = errorFunction # the error function (reversely proportionl to fitness)
		self.keep = elitism  # number of units to keep for elitism
		self.k = mutationScale # scale of the gaussian noise

		self.i = 0 # iteration counter

		# initialize the population randomly from a gaussian distribution
		# with noise 0.1 and then sort the values and store them internally

		self.population = []
		for _ in range(populationSize):
			chromosome = np.random.randn(chromosomeShape) * 0.1

			fitness = self.calculateFitness(chromosome)
			self.population.append((chromosome, fitness))

		# sort descending according to fitness (larger is better)
		self.population = sorted(self.population, key=lambda t: -t[1])
	
	def step(self):	
		"""
			Run one iteration of the genetic algorithm. In a single iteration,
			you should create a whole new population by first keeping the best
			units as defined by elitism, then iteratively select parents from
			the current population, apply crossover and then mutation.

			The step function should return, as a tuple: 
				
			* boolean value indicating should the iteration stop (True if 
				the learning process is finished, False othwerise)
			* an integer representing the current iteration of the 
				algorithm
			* the weights of the best unit in the current iteration

		"""
		#print "i ", self.i, " pop ", self.population[1]
		self.i += 1 

		done = False
		if self.i >= self.numIter:
			done = True
		
		new_gen = []
		offset = self.keep
		for it in range(offset):
			new_gen.append(self.population[it])

		while len(new_gen) < self.populationSize:
			p1, p2 = self.selectParents()
			new = self.crossover(p1, p2)
			new = self.mutate(new)
			new_gen.append((new, self.calculateFitness(new)))
		
		new_gen = sorted(new_gen, key=lambda t: -t[1])
		self.population = new_gen

		return (done, self.i, self.population[0][0])


	def calculateFitness(self, chromosome):
		"""
			Implement a fitness metric as a function of the error of
			a unit. Remember - fitness is larger as the unit is better!
		"""
		chromosomeError = self.f(chromosome)
		return -chromosomeError

	def bestN(self, n):		
		"""
			Return the best n units from the population
		"""
		best = []
		for i in range(n):
			best.append(self.population[i][0])
		return best

	def best(self):
		"""
			Return the best unit from the population
		"""
		return self.population[0][0]

	def selectParents(self):
		"""
			Select two parents from the population with probability of 
			selection proportional to the fitness of the units in the
			population		
		"""

		#roulette selection radimo
		weight_sum = 0.0
		for candidate in self.population:
			weight_sum -= candidate[1]		#fitness je negativan, jer je greska pohranjena pozitivna

		p1 = self.population[0][0]
		p2 = self.population[0][0]

		rand = np.random.random() * weight_sum
		temp_sum = weight_sum
		for candidate in self.population:
			temp_sum -= candidate[1]
			if temp_sum >= rand:
				p1 = candidate[0]
				break
		
		rand = np.random.random() * weight_sum

		temp_sum = weight_sum
		for candidate in self.population:
			temp_sum -= candidate[1]
			if temp_sum >= rand:
				p2 = candidate[0]
				break
		
		#if p1 is None:
		#	p1 = self.population[0][0]
		#if p2 is None:
		#	p2 = self.population[0][0]

		return p1, p2

	def crossover(self, p1, p2): 
		"""
			Given two parent units p1 and p2, do a simple crossover by 
			averaging their values in order to create a new child unit
		"""
		child = []
		for i in range(len(p1)):
			child.append((p1[i] + p2[i])/2.0)
		return np.array(child)

	def mutate(self, chromosome):
		"""
			Given a unit, mutate its values by applying gaussian noise
			according to the parameter k
		"""
		for i in range(len(chromosome)):
			if np.random.random() < self.p:
				chromosome[i] += np.random.normal(0, self.k)
		return chromosome 
